#### Adwords with Zend Framework 2 and DoctrineORM

##### Install 

    [X] create virtual host
    [X] clone repository or extract archive
    [X] php composer.phar install from root
    [x] import Mysql file from data/sql folder
    [x] change credentials in local.php, doctrine.global.php and bjyprofiler.global.php files 

##### Generate Cost

    [X] http://<DOMAIN>/api/generate-cost/<CAMPAIGN_ID>/<DATE> ( YYYY-m-d format)
    [X] ex : http://adwords.local/api/generate-cost/1/2020-01-27
    

##### Check Campaign History / Update Campaign budget
    
    [X] http://<DOMAIN>/