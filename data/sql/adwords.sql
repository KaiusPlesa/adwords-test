-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2020 at 09:54 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adwords`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE `campaign` (
  `id` mediumint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `start_time` int(10) NOT NULL,
  `end_time` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`id`, `name`, `start_time`, `end_time`, `status`) VALUES
  (1, 'Demo', 1546300800, 1551398400, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cost`
--

CREATE TABLE `cost` (
  `id` bigint(19) NOT NULL,
  `date` int(10) NOT NULL,
  `amount` mediumint(7) NOT NULL,
  `campaign` mediumint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cost`
--

INSERT INTO `cost` (`id`, `date`, `amount`, `campaign`) VALUES
  (13, 1580083200, 2, 1),
  (14, 1580089710, 13, 1),
  (15, 1580096883, 22, 1),
  (16, 1580103544, 5, 1),
  (17, 1580108505, 1, 1),
  (18, 1580115559, 0, 1),
  (19, 1580122435, 0, 1),
  (20, 1580127994, 1, 1),
  (21, 1579996800, 27, 1),
  (22, 1580001014, 3, 1),
  (23, 1546387200, 11, 1),
  (24, 1546394068, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` bigint(19) NOT NULL,
  `date` int(10) NOT NULL,
  `budget` mediumint(7) NOT NULL,
  `campaign` mediumint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `date`, `budget`, `campaign`) VALUES
  (1, 1546336800, 7, 1),
  (2, 1546340400, 0, 1),
  (3, 1546344000, 1, 1),
  (4, 1546383600, 6, 1),
  (5, 1546682400, 2, 1),
  (6, 1546732800, 0, 1),
  (7, 1549717980, 1, 1),
  (8, 1551441600, 0, 1),
  (9, 1551448800, 1, 1),
  (10, 1580036273, 0, 1),
  (11, 1580036302, 0, 1),
  (12, 1580036363, 0, 1),
  (13, 1580036424, 0, 1),
  (14, 1580036661, 0, 1),
  (15, 1580036784, 0, 1),
  (16, 1580036786, 0, 1),
  (17, 1580060201, 0, 1),
  (18, 1580060204, 0, 1),
  (19, 1580066330, 0, 1),
  (20, 1580066333, 0, 1),
  (21, 1580066889, 0, 1),
  (22, 1580066902, 12, 1),
  (23, 1580066925, 0, 1),
  (24, 1580066930, 15, 1),
  (25, 1580143543, 22, 1),
  (26, 1580148243, 1, 1),
  (27, 1580148255, 4, 1),
  (28, 1580153633, 0, 1),
  (29, 1580153817, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `budget_date` (`date`),
  ADD KEY `campaign` (`campaign`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `budget_date` (`date`),
  ADD KEY `campaign` (`campaign`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` mediumint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cost`
--
ALTER TABLE `cost`
  MODIFY `id` bigint(19) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` bigint(19) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `campaign` FOREIGN KEY (`campaign`) REFERENCES `campaign` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
