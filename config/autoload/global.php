<?php

$dbParams = array(
    'database'  => 'DATABASE',
    'username'  => 'USERNAME',
    'password'  => 'PASSWORD',
    'hostname'  => 'localhost',
    'options' => array('buffer_results' => true)
);

return array(
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => function ($sm) use ($dbParams) {
                $adapter = new BjyProfiler\Db\Adapter\ProfilingAdapter(array(
                    'driver'    => 'pdo',
                    'dsn'       => 'mysql:dbname='.$dbParams['database'].';host='.$dbParams['hostname'],
                    'database'  => $dbParams['database'],
                    'username'  => $dbParams['username'],
                    'password'  => $dbParams['password'],
                    'hostname'  => $dbParams['hostname'],
                ));

                if (php_sapi_name() == 'cli') {
                    $logger = new Zend\Log\Logger();
                    $writer = new Zend\Log\Writer\Stream('php://output');
                    $logger->addWriter($writer, Zend\Log\Logger::DEBUG);
                    $adapter->setProfiler(new BjyProfiler\Db\Profiler\LoggingProfiler($logger));
                } else {
                    $adapter->setProfiler(new BjyProfiler\Db\Profiler\Profiler());
                }
                if (isset($dbParams['options']) && is_array($dbParams['options'])) {
                    $options = $dbParams['options'];
                } else {
                    $options = array();
                }
                $adapter->injectProfilingStatementPrototype($options);
                return $adapter;
            },
        ),
    ),
    'rb_comment' => array(
        'default_visibility' => 1,
        'strings' => array(
            'author' => 'Author',
            'contact' => 'Email',
            'content' => 'Comment',
            'submit' => 'Post',
            'comments' => 'Comments',
            'required' => 'All fields are required. Contact info will not be published.',
            'signout' => 'Sign Out',
            'signin' => 'Sign In',
            'signedinas' => 'You are signed in as',
            'notsignedin' => 'You are not signed in. To be able to comment, please ',
        ),
        'email' => array(
            'notify' => true,
            'to' => array('office@historydocumentaries.info'),
            'from' => 'office@historydocumentaries.info',
            'subject' => 'New Comment',
            'context_link_text' => 'See this comment in context',
        ),
        'akismet' => array(
            'enabled' => true,
            'api_key' => 'APIKEY',
            'proxy' => array(
                'use' => false,
                'trusted' => array(
                ),
                'header' => 'X-Forwarded-For',
            ),
        ),
        'zfc_user' => array(
            'enabled' => true,
        ),
        'gravatar' => array(
            'enabled' => false,
        ),
    ),
);
