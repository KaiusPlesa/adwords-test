<?php

namespace Adwords\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Adwords\Traits;

class HistoryRepository extends EntityRepository
{
    use Traits\EntityManager;

    public function getLatestBudget($campaign = null)
    {
        $rsm = new ResultSetMapping();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('groupDate', 'groupDate');
        $rsm->addScalarResult('budget', 'budget');
        $rsm->addScalarResult('highest', 'highest');
        $rsm->addScalarResult('lowest', 'lowest');

        $rsmCost = new ResultSetMapping();
        $rsmCost->addScalarResult('cost', 'cost');
        $rsmCost->addScalarResult('costDate', 'costDate');

        $queryCost = EntityRepository::getEntityManager()
            ->createNativeQuery(
                "SELECT SUM(cost.amount) as cost, 
                     DATE(FROM_UNIXTIME(cost.date)) as costDate
                     FROM cost           
                     GROUP BY DATE(FROM_UNIXTIME(cost.date))",
                $rsmCost
            )
            ->setParameter(1, $campaign);

        $cost = [];
        foreach ($queryCost->getResult() as $key=>$value) {
            $cost[$value['costDate']]['cost'] = $value['cost'];
        }
        $query = EntityRepository::getEntityManager()
            ->createNativeQuery(
                "SELECT MAX(history.budget) as highest, 
                    MIN(history.budget) as lowest, 
                    DATE (FROM_UNIXTIME(history.date)) as groupDate 
                    FROM history 
                    WHERE history.campaign = ?
                    GROUP BY DATE(FROM_UNIXTIME(history.date))
                    ORDER BY history.date ASC",
                $rsm
            )
            ->setParameter(1, $campaign);

        $history = [];
        foreach ($query->getResult() as $key=>$value) {
            $history[$value['groupDate']]['highest'] = $value['highest'];
            $history[$value['groupDate']]['lowest'] = $value['lowest'];
            $history[$value['groupDate']]['groupDate'] = $value['groupDate'];
            $history[$value['groupDate']]['cost'] = 0;
        }

        $results = [];
        foreach ($history as $key=>$value) {
            $results[$value['groupDate']]['groupDate'] = $value['groupDate'];
            $results[$value['groupDate']]['highest'] = $value['highest'];
            $results[$value['groupDate']]['lowest'] = $value['lowest'];
            $results[$value['groupDate']]['cost'] = array_key_exists($value['groupDate'],$cost) ? $cost[$value['groupDate']]['cost']: 0;
        }

        return $results;

    }

    public function getLatestBudgetByDate($campaign = null, $date = null)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('budget', 'budget');

        $query = EntityRepository::getEntityManager()
            ->createNativeQuery(
                "SELECT budget 
                    FROM history WHERE campaign = ? AND DATE(FROM_UNIXTIME(date)) = ? 
                    ORDER BY date DESC LIMIT 1",
                $rsm
            )
            ->setParameter(1, $campaign)
            ->setParameter(2, $date);

        return $query->getResult();

    }

}

