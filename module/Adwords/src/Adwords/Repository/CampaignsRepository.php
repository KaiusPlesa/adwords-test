<?php

namespace Adwords\Repository;

use Doctrine\ORM\EntityRepository;


class CampaignsRepository extends EntityRepository
{
    public function getCampaignsDetails()
    {

        $result = $this->createQueryBuilder('campaigns')
            ->select('h, c')
            ->from('Adwords\Entity\History', 'h')
            ->join('Adwords\Entity\Campaign', 'c')
            ->where('c.id = h.campaign')
            ->getQuery();

        return $result->getResult();

    }

}
