<?php

namespace Adwords\Traits;

use Zend\ServiceManager\ServiceLocatorAwareTrait;

trait EntityManager
{
    use ServiceLocatorAwareTrait;

    protected function getEntityManager()
    {
        return $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }
}