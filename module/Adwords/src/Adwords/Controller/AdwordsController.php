<?php

namespace Adwords\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Adwords\Traits;
use Zend\View\Model\ViewModel;

class AdwordsController extends AbstractActionController
{
    use Traits\EntityManager;

    public function campaignHistoryAction()
    {
        $campaignId = $this->params()->fromRoute('id');

        $campaign = $this->getEntityManager()
            ->getRepository('Adwords\Entity\Campaign')
            ->findOneBy(
                ['id' => $campaignId]
            );

        $history = null;
        if ($campaign) {
            $history = $this->getEntityManager()
                ->getRepository('Adwords\Entity\History')
                ->getLatestBudget($campaignId);

        }

        return new ViewModel([
                'data' => [
                    "campaign" => $campaign,
                    "history" => $history
                ]
            ]
        );
    }

    public function campaignAction()
    {

        $campaigns = $this->getEntityManager()->getRepository('Adwords\Entity\Campaign')->findAll();

        return new ViewModel([
                'data' => [
                    "campaigns" => $campaigns
                ]
            ]
        );
    }


}
