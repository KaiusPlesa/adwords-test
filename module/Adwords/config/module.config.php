<?php

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => 'Adwords\Controller\Adwords',
                        'action' => 'campaign',
                    ],
                ],
            ],
            'adwords' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/adwords[/:action][/:id]',
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => 'Adwords\Controller\Adwords',
                        'action' => 'campaign',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [
            'Adwords\Controller\Adwords' => 'Adwords\Controller\AdwordsController',
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => false,
        'display_exceptions' => false,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            'Adwords' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'doctrine' => [
        'driver' => [
            'application_entities' => [
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Adwords/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Adwords\Entity' => 'application_entities'
                ]
            ]
        ]
    ],
];