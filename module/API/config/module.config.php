<?php

return [
    'router' => [
        'routes' => [
            'api' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/api[/:action][/:id][/:date]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => 'API\Controller\Api',
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [
            'API\Controller\Api' => 'API\Controller\ApiController',
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => false,
        'display_exceptions' => false,
        'doctype' => 'HTML5',
        'template_path_stack' => [
            'API' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];