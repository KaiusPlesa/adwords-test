<?php

namespace API\Controller;

use Adwords\Entity\History;
use Adwords\Entity\Cost;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use API\Traits;

class ApiController extends AbstractActionController
{
    use Traits\EntityManager;

    public function indexAction()
    {
        return new JsonModel([
                'data' => [
                    "page" => 'index',
                    "version" => '1.0.0'
                ]
            ]
        );
    }

    public function updateCampaignStatusAction()
    {
        $campaignId = $this->params()->fromPost('id');
        $status = $this->params()->fromPost('status');

        $campaign = $this->getEntityManager()->getRepository('Adwords\Entity\Campaign')->findOneBy(['id' => $campaignId]);
        $campaign->setStatus($status);
        $this->getEntityManager()->flush();

        $history = new History();
        $history->setDate(time());
        $history->setCampaign($campaign);

        if ($status) {
            $latestBudget = $this->getEntityManager()
                ->getRepository('Adwords\Entity\History')
                ->findOneBy(
                    [
                        'campaign' => $campaignId
                    ],
                    ['date' => 'DESC']
                );
            $history->setBudget($latestBudget->getBudget());
        } else {
            $history->setBudget(0);
        }

        $this->getEntityManager()->persist($history);
        $this->getEntityManager()->flush();

        return new JsonModel([
                'data' => [
                    "message" => 'Updated',
                    "version" => '1.0.0'
                ]
            ]
        );
    }

    public function getBudgetAction()
    {
        $campaignId = $this->params()->fromPost('id');

        $latestBudget = $this->getEntityManager()
            ->getRepository('Adwords\Entity\History')
            ->findOneBy(
                [
                    'campaign' => $campaignId,
                    'status' => 1
                ],
                ['date' => 'DESC']
            );
        return new JsonModel([
                'data' => [
                    "budget" => $latestBudget->getBudget()
                ]
            ]
        );
    }

    public function updateCampaignBudgetAction()
    {
        $campaignId = $this->params()->fromPost('id');
        $budget = $this->params()->fromPost('budget');

        $campaign = $this->getEntityManager()->getRepository('Adwords\Entity\Campaign')->findOneBy(['id' => $campaignId]);

        $history = new History();
        $history->setDate(time());
        $history->setCampaign($campaign);
        $history->setBudget($budget);

        $this->getEntityManager()->persist($history);
        $this->getEntityManager()->flush();

        return new JsonModel([
                'data' => [
                    "message" => 'Updated',
                    "version" => '1.0.0'
                ]
            ]
        );
    }

    public function generateCostAction()
    {
        $campaignId = $this->params()->fromRoute('id');
        $date = $this->params()->fromRoute('date');

        $latestBudget = $this->getEntityManager()
            ->getRepository('Adwords\Entity\History')
            ->getLatestBudgetByDate($campaignId, $date);

        $campaign = $this->getEntityManager()->getRepository('Adwords\Entity\Campaign')->findOneBy(['id' => $campaignId, 'status' => 1]);
        $budget = $latestBudget[0]['budget'] ? (int)$latestBudget[0]['budget'] : 0;


        /**
         * Generate cost if campaign is active
         */
        if ($campaign) {
            /**
             * Generate Cost
             */
            $costs = [];
            $time = strtotime($date);
            $maxBudget = 2 * $budget;
            for ($i = 1; $i <= 10; $i++) {
                $total = array_sum($costs);
                if (array_sum($costs) < $maxBudget) {
                    $costs[count($costs) ? end(array_keys($costs)) + rand(3600, 7200) : $time] = rand(0, ($maxBudget - $total));
                }
            }

            /**
             * Update History
             */
            foreach ($costs as $date => $cost) {
                $history = new Cost();
                $history->setDate($date);
                $history->setAmount($cost);
                $history->setCampaign($campaign);

                $this->getEntityManager()->persist($history);
                $this->getEntityManager()->flush();
            };

            return new JsonModel([
                    'data' => [
                        "message" => 'Cost generated',
                        "version" => '1.0.0'
                    ]
                ]
            );
        } else {
            return new JsonModel([
                    'data' => [
                        "message" => 'Campaign is paused',
                        "version" => '1.0.0'
                    ]
                ]
            );
        }
    }

}
